import {Component, Input, OnInit} from '@angular/core';
import {Character} from '../../models/character.model';

@Component({
  selector: 'advanced-graph',
  templateUrl: './advanced-graph.component.html',
  styleUrls: ['./advanced-graph.component.scss']
})
export class AdvancedGraphComponent implements OnInit {
  @Input() characters: Character[] = [];
  range: number[];

  constructor() { }

  ngOnInit() {
    this.generateYearRange();
    this.getCharactersYoungerThan100BBY();
  }

  generateYearRange(): void{
    this.range = Array(11).fill(1).map((x, i) => i * 10);
  }

  getCharactersYoungerThan100BBY(): void {
    this.characters = this.characters.filter(x => x.birth_year != null && x.average_lifespan != null && x.birth_year < 100);
  }

  calculatePaddingForCharacter(bby: number): number {
    return bby * 0.909;
  }

}
