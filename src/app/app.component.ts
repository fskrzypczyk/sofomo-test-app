import {Component, OnInit} from '@angular/core';
import {StarWarsService} from './services/star-wars.service';
import {Character} from './models/character.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  characters: Character[] = [];

  constructor(private starWarsService: StarWarsService) { }

  ngOnInit(): void {
    this.starWarsService.getCharacters().then(res => this.characters = res);
  }
}
