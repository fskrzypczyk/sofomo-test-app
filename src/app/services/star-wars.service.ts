import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Character } from '../models/character.model';
import {SwapiResponse} from '../interfaces/swapi-response.interface';
import {SwapiSpiece} from '../interfaces/swapi-spiece.inteface';
import {SwapiCharacter} from '../interfaces/swapi-character.interface';

@Injectable({
  providedIn: 'root'
})
export class StarWarsService {
  private charactersArray: Character[] = [];
  private speciesObj: { [index: string]: any } = {};

  constructor(private httpClient: HttpClient) { }

  getCharacters(): Promise<Character[]>{
    return new Promise((resolve, reject) => this.fetchCharactersFromAllPages(null, resolve, reject));
  }

  private fetchCharactersFromAllPages(link, resolve, reject): void {
    this.httpClient.get<SwapiResponse>(link || "https://swapi.co/api/people/?format=json").toPromise().then(response => {
      let characters: SwapiCharacter[] = response.results;
      this.fetchUniqueSpieces(characters).then(() => characters.forEach(c => this.charactersArray.push(new Character(c, this.speciesObj[c.species[0]]))));

      let nextPageLink = response.next;
      if (nextPageLink !== null) {
        this.fetchCharactersFromAllPages(nextPageLink, resolve, reject);
      } else {
        this.charactersArray = this.charactersArray.sort((a, b) => (a.birth_year > b.birth_year) ? 1 : ((b.birth_year > a.birth_year) ? -1 : 0));
        resolve(this.charactersArray);
      }
    }).catch(error => {
      console.error(error);
      reject(error);
    });
  }

  private fetchAvgLifespanBySpiece(link): Promise<void> {
    if(this.speciesObj[link] !== undefined || link === undefined) return null;

    return new Promise(resolve => {
      this.httpClient.get<SwapiSpiece>(link).toPromise().then(x => {
        let avgLifespan = x.average_lifespan;
        this.speciesObj[link] = avgLifespan == "indefinite" ? 9999999 : parseFloat(avgLifespan) || null;
        resolve();
      })
    });
  }

  private fetchUniqueSpieces(characters: SwapiCharacter[]){
    let promisesArr: Promise<void>[] = [];
    new Set(characters.map(c => c.species[0])).forEach(x => promisesArr.push(this.fetchAvgLifespanBySpiece(x)));

    return Promise.all(promisesArr);
  }
}
