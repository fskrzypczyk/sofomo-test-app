import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { SimpleGraphComponent } from './components/simple-graph/simple-graph.component';
import { AdvancedGraphComponent } from './components/advanced-graph/advanced-graph.component';
import { PreloaderComponent } from './components/preloader/preloader.component';

@NgModule({
  declarations: [
    AppComponent,
    SimpleGraphComponent,
    AdvancedGraphComponent,
    PreloaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
