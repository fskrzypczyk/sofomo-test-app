import {SwapiCharacter} from '../interfaces/swapi-character.interface';

export class Character {
  constructor(c: SwapiCharacter, avgLifespan: number) {
    this.name = c.name;
    this.height = this.convertStringToNumber(c.height);
    this.mass = this.convertStringToNumber(c.mass);
    this.birth_year = parseFloat(c.birth_year.replace("BBY", "")) || null;
    this.average_lifespan = avgLifespan;
  }

  name: string;
  height: number;
  mass: number;
  birth_year: number;
  average_lifespan: number;

  get bmi(): number {
    if(!this.height || !this.mass) return null;
      let bmiVal = (this.mass / Math.pow(this.height / 100, 2)).toFixed(2);
      return parseFloat(bmiVal);
  }

  get badgeClass(): string {
    let badgeColor = "";
    if(this.bmi < 16) badgeColor = "dark";
    if(this.bmi >= 16 && this.bmi < 24.99) badgeColor = "success";
    if(this.bmi >= 25 && this.bmi < 39.99) badgeColor = "warning";
    if(this.bmi > 40 || this.bmi == null) badgeColor = "danger";

    return `badge badge-${badgeColor}`;
  }

  private convertStringToNumber(val: string): number {
    return parseFloat(val.replace(",", ".")) || null;
  }
}
