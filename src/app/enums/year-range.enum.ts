export enum YearRange {
  BBY0_BBY20,
  BBY20_BBY40 ,
  BBY40_BBY60,
  BBY60_BBY80,
  BBY80_BBY100,
  BBY100,
  Unknown
}
