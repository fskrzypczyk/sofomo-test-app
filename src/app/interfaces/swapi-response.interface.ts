import {SwapiCharacter} from './swapi-character.interface';

export interface SwapiResponse {
  count: number;
  next: string;
  previous: string;
  results: SwapiCharacter[];
}
