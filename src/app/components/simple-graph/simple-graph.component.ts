import {Component, Input, OnInit} from '@angular/core';
import {Character} from '../../models/character.model';
import {YearRange} from '../../enums/year-range.enum';

@Component({
  selector: 'simple-graph',
  templateUrl: './simple-graph.component.html',
  styleUrls: ['./simple-graph.component.scss']
})
export class SimpleGraphComponent implements OnInit {
  @Input() characters: Character[] = [];
  charactersByBirthYear: { [index: string]: Character[] } = {};

  constructor() { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    this.getCharactersByBirthYear().then(res => {
      this.charactersByBirthYear = res;
    });
  }

  getCharactersByBirthYear(): Promise<{ [index: number]: Character[] }>{
    return new Promise(resolve => {
        let bbyObj = {};

        bbyObj[YearRange.BBY0_BBY20] = this.characters.filter(x => x.birth_year > 0 && x.birth_year < 20);
        bbyObj[YearRange.BBY20_BBY40] = this.characters.filter(x => x.birth_year >= 20 && x.birth_year < 40);
        bbyObj[YearRange.BBY40_BBY60] = this.characters.filter(x => x.birth_year >= 40 && x.birth_year < 60);
        bbyObj[YearRange.BBY60_BBY80] = this.characters.filter(x => x.birth_year >= 60 && x.birth_year < 80);
        bbyObj[YearRange.BBY80_BBY100] = this.characters.filter(x => x.birth_year >= 80 && x.birth_year < 100);
        bbyObj[YearRange.BBY100] = this.characters.filter(x => x.birth_year >= 100);
        bbyObj[YearRange.Unknown] = this.characters.filter(x => x.birth_year == null);

        resolve(bbyObj);
      });
    }
}
